import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class PageContentService {


    store(data) {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/api/page-contents', data);

        return promise;
    }

    update(data) {
        promise = axios.put(Config.base_url + Config.admin_prefix + '/api/page-contents/' + data.id, data);

        return promise;
    }

    list(page) {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/page-contents?page=' + page);

        return promise;
    }

    show(id) {
        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/page-contents/' + id);

        return promise;
    }

    delete(id) {
        promise = axios.delete(Config.base_url + Config.admin_prefix + '/api/page-contents/' + id);

        return promise;
    }

}