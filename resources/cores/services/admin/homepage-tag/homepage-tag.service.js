import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class HomepageTagService {


    store(data) {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/api/homepage-tag', data);

        return promise;
    }

    list() {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/homepage-tag');

        return promise;
    }

}
