import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class AboutService {

    list() {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/about');

        return promise;
    }

    store(data) {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/api/about', data);

        return promise;
    }

}
