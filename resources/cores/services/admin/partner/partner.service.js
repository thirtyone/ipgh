import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class PartnerService {


    store(data) {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/api/partners', data);

        return promise;
    }

    update(data) {
        promise = axios.put(Config.base_url + Config.admin_prefix + '/api/partners/' + data.id, data);

        return promise;
    }

    list(data) {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/partners', {params: data});

        return promise;
    }

    show(id) {
        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/partners/' + id);

        return promise;
    }

    delete(id) {
        promise = axios.delete(Config.base_url + Config.admin_prefix + '/api/partners/' + id);

        return promise;
    }

}