import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class CompanyService {


    store(data) {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/api/companies', data, {async: false});

        return promise;
    }

    update(data) {
        promise = axios.put(Config.base_url + Config.admin_prefix + '/api/companies/' + data.id, data);

        return promise;
    }

    list(data) {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/companies', {params: data});

        return promise;
    }

    show(id) {
        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/companies/' + id);

        return promise;
    }

    delete(id) {
        promise = axios.delete(Config.base_url + Config.admin_prefix + '/api/companies/' + id);

        return promise;
    }

}