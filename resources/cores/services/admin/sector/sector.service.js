import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class SectorService {


    store(data) {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/api/sectors', data);

        return promise;
    }

    update(data) {
        promise = axios.put(Config.base_url + Config.admin_prefix + '/api/sectors/' + data.id, data);

        return promise;
    }

    list(data) {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/sectors', {params: data});

        return promise;
    }

    show(id) {
        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/sectors/' + id);

        return promise;
    }

    delete(id) {
        promise = axios.delete(Config.base_url + Config.admin_prefix + '/api/sectors/' + id);

        return promise;
    }

}