import axios from "axios";
import Config from "../../../../configs/app.config";

var promise;


export default class AuthService {


    logout() {

        promise = axios.post(Config.base_url + Config.admin_prefix + '/sign-out');
        return promise;
    }



}