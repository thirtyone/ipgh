import axios from "axios";
import Config from "../../../configs/app.config";

var promise;


export default class OptionService {


    pageContentOption() {

        promise = axios.get(Config.base_url + Config.base_api_prefix + '/option/get-page-content-options');

        return promise;


    }

}