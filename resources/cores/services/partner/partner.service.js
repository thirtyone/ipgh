import axios from "axios";
import Config from "../../../configs/app.config";


var promise;


export default class PartnerService {


    list() {

        promise = axios.get(Config.base_url + '/api/partners');

        return promise;
    }

    show(slug) {

        promise = axios.get(Config.base_url + '/api/partners/' + slug);

        return promise;
    }

}