import axios from "axios";
import Config from "../../../configs/app.config";

var promise;


export default class AboutService {

    list() {

        promise = axios.get(Config.base_url + '/api/about');

        return promise;
    }

}
