import axios from "axios";
import Config from "../../../configs/app.config";

var promise;


export default class HomepageTagService {

    list() {

        promise = axios.get(Config.base_url + Config.admin_prefix + '/api/homepage-tag');

        return promise;
    }

}
