import axios from "axios";
import Config from "../../../configs/app.config";


var promise;


export default class NewsPressService {


    list() {

        promise = axios.get(Config.base_url + '/api/news-presses');

        return promise;
    }

    show(slug) {

        promise = axios.get(Config.base_url + '/api/news-presses/' + slug);

        return promise;
    }

    listWithPagination(id, page, limit) {
        promise = axios.get(Config.base_url + '/api/news-presses/with?id=' + id + '&page=' + page + '&limit=' + limit);

        return promise;
    }

}