import axios from "axios";
import Config from "../../../configs/app.config";


var promise;


export default class SectorService {


    list() {

        promise = axios.get(Config.base_url + '/api/sectors');

        return promise;
    }

    show(slug) {

        promise = axios.get(Config.base_url + '/api/sectors/' + slug);

        return promise;
    }

}