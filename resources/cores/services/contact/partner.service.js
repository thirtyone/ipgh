import axios from "axios";
import Config from "../../../configs/app.config";


var promise;


export default class ContactService {


    store(data) {

        promise = axios.post(Config.base_url + '/api/contacts',data);

        return promise;
    }


    list() {

        promise = axios.get(Config.base_url + '/api/contacts');

        return promise;
    }

    show(slug) {

        promise = axios.get(Config.base_url + '/api/contacts/' + slug);

        return promise;
    }

}