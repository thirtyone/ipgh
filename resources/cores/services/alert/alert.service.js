
export default class AlertService {

    saved (){


        $.notify({
            icon: "fa fa-check-circle",
            message: "<b>Success</b> <br> Record Successfully Saved."

        }, {
            type: 'success',
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });


    }



    updated (){


        $.notify({
            icon: "nc-icon nc-check-2",
            message: "<b>Success</b> <br> Record Successfully Updated."

        }, {
            type: 'success',
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });


    }

    submitWithMessage (message){


        $.notify({
            icon: "fa fa-check-circle",
            message: "<b>Success</b> <br> " + message

        }, {
            type: 'success',
            timer: 2000,
            placement: {
                from: 'top',
                align: 'center'
            }
        });


    }



    deleted (){


        $.notify({
            icon: "nc-icon nc-check-2",
            message: "<b>Success</b> <br> Record Successfully Deleted."

        }, {
            type: 'success',
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });


    }


    errorWithMessage (message){


        $.notify({
            icon: "fa fa-times",
            message: "<b>Error</b> <br> " + message

        }, {
            type: 'danger',
            timer: 2000,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });


    }


    uploading (){


        $.notify({
            icon: "nc-icon nc-cloud-upload-94",
            message: "<b>Loading..</b> <br> Please Wait."

        }, {
            type: 'primary',
            allow_dismiss: false,
            timer: 800000,
            onClosed: null,
            placement: {
                from: 'bottom',
                align: 'right'
            }
        });


    }


    uploadSuccess(){


        $.notifyClose();



        setTimeout(function () {
            $.notify({
                icon: "nc-icon nc-check-2",
                message: "<b>Success</b> <br> File Successfully Uploaded."

            }, {
                type: 'success',
                timer: 2000,
                placement: {
                    from: 'bottom',
                    align: 'right'
                }
            });
        },800);

    }

}