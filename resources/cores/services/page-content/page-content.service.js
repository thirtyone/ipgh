import axios from "axios";
import Config from "../../../configs/app.config";
import Constant from "../../../configs/app.constant";

var promise;


export default class PageContentService {


    list() {

        promise = axios.get(Config.base_url + '/api/page-contents');

        return promise;
    }

    show(id) {

        promise = axios.get(Config.base_url + '/api/page-contents/' + id);

        return promise;
    }

}