import axios from "axios";
import Config from "../../../configs/app.config";

var promise;


export default class CompanyService {


    list() {

        promise = axios.get(Config.base_url + '/api/companies');

        return promise;
    }

    show(slug) {

        promise = axios.get(Config.base_url + '/api/companies/' + slug);

        return promise;
    }

}