
// import App from './app.component';
// import HomeComponent from './components/home/home.component'
import AppComponent from './app.component';
import MissionAndVisionComponent from './components/mission-and-vision/mission-and-vision.component';
import ProjectComponent from './components/project/project.component';
import SectorComponent from './components/sector/sector.component';
import AboutComponent from './components/about/about.component';
import PartnerComponent from './components/partner/partner.component';
import ContactComponent from './components/contact/contact.component';
import NewsAndPressComponent from './components/news-and-press/news-and-press.component';
import NewsAndPressDetailComponent from './components/news-and-press/news-and-press-detail.component';
import CompanyListComponent from './components/company/company-list.component';
import CompanyComponent from './components/company/company.component';

export default  [
    {
        name: 'app-component',
        path: "/",
        component: AppComponent
    },
    // {
    //     name: 'mission-and-vision',
    //     path: "/mission-and-vision",
    //     component: MissionAndVisionComponent
    // },
    // {
    //     name: 'project-component',
    //     path: "/projects",
    //     component: ProjectComponent
    // },
    {
        name: 'sector-component',
        path: "/sectors",
        component: SectorComponent
    },
    {
        name: 'about-component',
        path: "/about",
        component: AboutComponent
    },
    {
        name: 'partner-component',
        path: "/partners",
        component: PartnerComponent
    },
    {
        name: 'company-list-component',
        path: "/companies",
        component: CompanyListComponent
    },
    {
        name: 'company-component',
        path: "/company/:slug",
        component: CompanyComponent
    },
    {
        name: 'news-and-press-component',
        path: "/news-and-press",
        component: NewsAndPressComponent
    },
    {
        name: 'news-and-press-detail-component',
        path: "/news-and-press/:slug",
        component: NewsAndPressDetailComponent
    },
    {
        name: 'contact-component',
        path: "/contact",
        component: ContactComponent
    },

];