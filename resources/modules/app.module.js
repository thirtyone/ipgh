/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('../assets/bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import AppRouter from './app.router';
// import routes from './app.router';.
import NavBar from './../cores/includes/navbar.component';
import footerBar from './../cores/includes/footer.component';
Vue.use(VueRouter);
Vue.component('navbar-component', NavBar);
Vue.component('footer-component', footerBar);

const router = new VueRouter(
    {
        mode: 'history',
        routes: AppRouter
    });

const app = new Vue(Vue.util.extend({router})).$mount('#app');
