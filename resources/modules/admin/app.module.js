/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('../../assets/bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import AppRouter from './app.router';
// import routes from './app.router';
import SideBar from '../../cores/admin/includes/sidebar.component'
import NavBar from '../../cores/admin/includes/navbar.component'
Vue.use(VueRouter);
Vue.component('sidebar-component', SideBar);
Vue.component('navbar-component', NavBar);


const router = new VueRouter(
    {
        mode: 'history',
        routes: AppRouter
    });
const app = new Vue(Vue.util.extend({router})).$mount('#app');
