import Config from '../../configs/app.config';
import App from './app.component';
import ListPartnerComponent from './components/partner/list-partner.component';
import CrupPartnerComponent from './components/partner/crup-partner.component';

import ListCompanyComponent from './components/company/list-company.component';
import CrupCompanyComponent from './components/company/crup-company.component';

import ListPageContentComponent from './components/page-content/list-page-content.component';
import CrupPageContentComponent from './components/page-content/crup-page-content.component';

import ListSectorComponent from './components/sector/list-sector.component';
import CrupSectorComponent from './components/sector/crup-sector.component';

import ListProjectComponent from './components/project/list-project.component';
import CrupProjectComponent from './components/project/crup-project.component';

import ListNewsPressComponent from './components/news-press/list-news-press.component';
import CrupNewsPressComponent from './components/news-press/crup-news-press.component';

import ListContactComponent from './components/contact/list-contact.component';
import ShowContactComponent from './components/contact/show-contact.component';

import CrupHomepageTagComponent from './components/homepage-tag/crup-homepage-tag.component';
import CrupAboutComponent from './components/about/crup-about.component';

export default  [
    {
        name: 'app',
        path: Config.admin_prefix,
        component: ListProjectComponent
    },
    {
        name: 'partner-list',
        path: Config.admin_prefix + '/partners',
        component: ListPartnerComponent
    },
    {
        name: 'partner-create',
        path: Config.admin_prefix + '/partners/create',
        component: CrupPartnerComponent
    },
    {
        name: 'partner-update',
        path: Config.admin_prefix + '/partners/:id/edit',
        component: CrupPartnerComponent
    },
    //company
    {
        name: 'company-list',
        path: Config.admin_prefix + '/companies',
        component: ListCompanyComponent
    },
    {
        name: 'company-create',
        path: Config.admin_prefix + '/companies/create',
        component: CrupCompanyComponent
    },
    {
        name: 'company-update',
        path: Config.admin_prefix + '/companies/:id/edit',
        component: CrupCompanyComponent
    },
    //page-content
    {
        name: 'page-content-list',
        path: Config.admin_prefix + '/page-contents',
        component: ListPageContentComponent
    },
    {
        name: 'page-content-create',
        path: Config.admin_prefix + '/page-contents/create',
        component: CrupPageContentComponent
    },
    {
        name: 'page-content-update',
        path: Config.admin_prefix + '/page-contents/:id/edit',
        component: CrupPageContentComponent
    },
    //sectors
    {
        name: 'sector-list',
        path: Config.admin_prefix + '/sectors',
        component: ListSectorComponent
    },
    {
        name: 'sector-create',
        path: Config.admin_prefix + '/sectors/create',
        component: CrupSectorComponent
    },
    {
        name: 'sector-update',
        path: Config.admin_prefix + '/sectors/:id/edit',
        component: CrupSectorComponent
    },
    //projects
    {
        name: 'project-list',
        path: Config.admin_prefix + '/projects',
        component: ListProjectComponent
    },
    {
        name: 'project-create',
        path: Config.admin_prefix + '/projects/create',
        component: CrupProjectComponent
    },
    {
        name: 'project-update',
        path: Config.admin_prefix + '/projects/:id/edit',
        component: CrupProjectComponent
    },
    //news and press
    {
        name: 'news-press-list',
        path: Config.admin_prefix + '/news-presses',
        component: ListNewsPressComponent
    },
    {
        name: 'news-press-create',
        path: Config.admin_prefix + '/news-presses/create',
        component: CrupNewsPressComponent
    },
    {
        name: 'news-press-update',
        path: Config.admin_prefix + '/news-presses/:id/edit',
        component: CrupNewsPressComponent
    },    //news and press
    {
        name: 'contact-list',
        path: Config.admin_prefix + '/contacts',
        component: ListContactComponent
    },
    {
        name: 'contact-show',
        path: Config.admin_prefix + '/contacts/:id',
        component: ShowContactComponent
    },
    //homepage tag
    {
        name: 'homepage-tag',
        path: Config.admin_prefix + '/homepage-tags',
        component: CrupHomepageTagComponent
    },
    //about
    {
        name: 'about',
        path: Config.admin_prefix + '/about',
        component: CrupAboutComponent
    },

];
