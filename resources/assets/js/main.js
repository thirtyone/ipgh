$('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});

$('.menu-dropdown').click(function () {
    $('nav.mobile').toggleClass('active');
});

$('header nav ul li a').click(function () {
    $('header nav ul li').removeClass('active');
    $(this).parent().addClass('active');
});

$('#project-snapshot .marker > .pin').click(function () {
    $('#project-snapshot .marker').removeClass('active');
    $(this).parent().addClass('active');
});

$('.comb a#selectTab').click(function () {
    $('.tabs > div').hide();
    var titleDesc = $(this).find('img').attr('title');
    $('.tabs > div').attr('id', titleDesc);
    $('.tabs > div').fadeIn();
});



// Hide Header on scroll down

if($( window ).width() > 768){
    var didScroll;
    var lastScrollTop = 0;
    var delta = 50;
    var navbarHeight = 100;


    $(window).scroll(function (event) {
        didScroll = true;

    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 0);

    function hasScrolled() {
        var st = $(this).scrollTop();

        if (st == 0) {
            $('header').removeClass('scrolledDown');
        }


        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st > lastScrollTop && st > navbarHeight) {
            // Scroll Down
            $('header').addClass('scrolledDown');
        }
        // else {
        //     // Scroll Up
        //     if (st + $(window).height() < $(document).height()) {

        //         $('header').addClass('scrolledDown');
        //     }
        // }
        lastScrollTop = st;
    }
    // End Hide Header
}

$(document).ready(function() {

    // light Slider
    var slider = $("#awards-slider").lightSlider({
        item: 5,
        pager: false,
        controls: false,
        loop: true,
        centerSlide:true,
        // prevHtml: '<i class="fa fa-chevron-circle-left fa-2x"></i>',
        // nextHtml: '<i class="fa fa-chevron-circle-right fa-2x"></i>',
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                }
            }
        ]
    });

    $('.slideControls .slidePrev').click(function() {
        slider.goToPrevSlide();
    });

    $('.slideControls .slideNext').click(function() {
        slider.goToNextSlide();
    });

});
