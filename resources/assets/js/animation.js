function imageShuffle() {
    TweenMax.staggerFrom(".hex-block .hex", 0.5, {
        scale: 0.5,
        opacity: 0,
        delay: 2,
        ease: Power0.easeNone
    }, 0.1);
}

imageShuffle();

function clickSub() {
    var subComp = new TimelineMax({});
    subComp.to($('._inner'), 1, {
        css: {
            opacity: "1",
            zIndex: "1"
        }
    }, 0)
        .to($('._outer'), 0.5, {
            css: {
                opacity: "0",
                zIndex: "-1"
            }
        }, 0)
}

$('.sub_companies').click(function () {
    $(this).addClass('active');
    clickSub();
});

function combStagger() {
    TweenMax.staggerTo(".comb .sub_comb", 0.2, {
        css: {
            opacity: "1"
        },
        delay: 4.3
    }, 0.5);
}

combStagger();

function scrollTotal() {
    scrollHeight = $('#home').height() + $('#mission').height() + $('#project-snapshot').height() + $('#vision').height() - 300;
    console.log(scrollHeight);
    $(window).scroll(function () {
        if ($(window).scrollTop() >= scrollHeight) {
            $('.vision-text-content').delay(3000).fadeOut(1000);
            $('.our-sector-content').delay(4200).fadeIn(1000);
        }

    });
}

// scrollTotal();

function backComp() {
    var parentComp = new TimelineMax({});
    parentComp.to($('._inner'), 1, {
        css: {
            opacity: "0",
            zIndex: "-1"
        }
    }, 0)
        .to($('._outer'), 0.5, {
            css: {
                opacity: "1",
                zIndex: "20"
            }
        }, 0)
}

$('._back').click(function () {
    $('.hex-block._outer .hex').removeClass('active');
    backComp();
});

/*Tweening*/
function introAnimation() {

    $('#home #home-banner').css("left", "-80%");
    $('nav').css("left", "-100%");

    TweenMax.to(".left-bottom-bg", 0.8, {
        css: {
            left: "2%",
            bottom: "2%",
            margin: "0"
        },
        delay: 0.5
    })
    TweenMax.to(".right-top-bg", 0.8, {
        css: {
            right: "2%",
            top: "2%",
            margin: "0"
        },
        delay: 0.5
    })
    TweenMax.to(".plus.top", 0.8, {
        css: {
            top: "2%",
            margin: "0"
        },
        delay: 0.5
    })
    TweenMax.to(".plus.bottom", 0.8, {
        css: {
            bottom: "2%",
            margin: "0"
        },
        delay: 0.5
    })
    TweenMax.to(".loader", 2, {
        css: {
            transform: "scale(4)",
            display: "none"
        },
        delay: 0.5
    })

    TweenMax.to(".blackbg", 1.5, {
        css: {
            opacity: "0",
            display: "none"

        },
        delay: 0.6
    })

    /*Home Animation*/
    TweenMax.to("#home-banner", 1, {
        css: {
            left: "0"
        },
        delay: 1.5
    })

    TweenMax.to(".logo", 0.8, {
        css: {
            opacity: "1"
        },
        delay: 3.5
    })


    TweenMax.to("nav", 0.8, {
        css: {
            left: "0"
        },
        delay: 2.3
    })

    TweenMax.to(".text-content", 0.8, {
        css: {
            opacity: "1",
            top: "0"
        },
        delay: 3
    })

}

function bgAnimation() {
    TweenMax.to(".plus.top", 0.2, {
        opacity: 0,
        ease: RoughEase.ease.config({
            strength: 0.3,
            points: 8,
            randomize: false
        }),
        repeat: -1,
        yoyo: true
    });
    TweenMax.to(".plus.bottom", 0.8, {
        opacity: 0,
        ease: RoughEase.ease.config({
            strength: 0.3,
            points: 2,
            randomize: true
        }),
        repeat: -1,
        yoyo: true
    });
    TweenMax.to(".left-bottom-bg", 2, {
        opacity: 0,
        ease: RoughEase.ease.config({
            strength: 0.3,
            points: 5,
            randomize: true
        }),
        repeat: -1,
        yoyo: true
    });
    TweenMax.to(".right-top-bg", 4, {
        opacity: 0,
        ease: RoughEase.ease.config({
            strength: 0.3,
            points: 6,
            randomize: false
        }),
        repeat: -1,
        yoyo: true
    });
}

function changePage() {

    TweenMax.to(".left-bottom-bg", 0.8, {
        css: {
            left: "50%",
            bottom: "50%",
            marginBottom: "-80px",
            marginLeft: "-104px"
        },
        delay: 0
    })

    TweenMax.to(".right-top-bg", 0.8, {
        css: {
            right: "50%",
            top: "50%",
            marginTop: "-80px",
            marginRight: "-104px"
        },
        delay: 0
    })

    TweenMax.to(".plus.top", 0.8, {
        css: {
            top: "50%",
            marginTop: "-7.5px"
        },
        delay: 0
    })

    TweenMax.to(".plus.bottom", 0.8, {
        css: {
            bottom: "50%",
            marginBottom: "-7.5px"
        },
        delay: 0
    })

    TweenMax.to(".loader", 0.7, {
        css: {
            transform: "scale(0)",
            display: "block"
        },
        delay: 0
    })

    TweenMax.to(".blackbg", 0.1, {
        css: {
            opacity: "1",
            display: "block"

        },
        delay: 0
    })

    TweenMax.to(".logo", 0.8, {
        css: {
            opacity: "0"
        },
        delay: 0.3
    })

    TweenMax.to("nav", 0.8, {
        css: {
            left: "-100%"
        },
        delay: 0.3
    })


    /*Going In Page*/

    TweenMax.to(".left-bottom-bg", 0.8, {
        css: {
            left: "2%",
            bottom: "2%",
            margin: "0"
        },
        delay: 1.3
    })

    TweenMax.to(".right-top-bg", 0.8, {
        css: {
            right: "2%",
            top: "2%",
            margin: "0"
        },
        delay: 1.3
    })

    TweenMax.to(".plus.top", 0.8, {
        css: {
            top: "2%",
            margin: "0"
        },
        delay: 1.3
    })

    TweenMax.to(".plus.bottom", 0.8, {
        css: {
            bottom: "2%",
            margin: "0"
        },
        delay: 1.3
    })

    TweenMax.to(".loader", 2, {
        css: {
            transform: "scale(4)",
            display: "none"
        },
        delay: 1.3
    })

    TweenMax.to(".blackbg", 3, {
        css: {
            opacity: "0",
            display: "none"

        },
        /*delay: 1.3*/
        delay: 1.3
    })

    TweenMax.to(".logo", 0.8, {
        css: {
            opacity: "1"
        },
        delay: 1.8
    })

    TweenMax.to("nav", 0.8, {
        css: {
            left: "0"
        },
        delay: 1.8
    })

}

/*Desktop Animations*/
var screen = $(window)
if (screen.width() > 768) {

    new WOW().init();
    introAnimation();
    bgAnimation();
    glitch();

}

TweenLite.set(".cardWrapper", {
    perspective: 800
});
TweenLite.set(".card", {
    transformStyle: "preserve-3d"
});
/* TweenLite.set(".rear", {
    rotationY: -180
});
TweenLite.set([".rear", ".front"], {
    backfaceVisibility: "hidden"
}); */
TweenLite.set(".rear", {
    rotationY: -180,
    zIndex: "1"
});
TweenLite.set([".rear",".front"], {
    backfaceVisibility: "hidden"
});

$(".cardWrapper").hover(
    function () {
        TweenLite.to($(this).find(".card"), 1.2, {
            rotationY: 180,
            ease: Back.easeOut
        });
    },
    function () {
        TweenLite.to($(this).find(".card"), 1.2, {
            rotationY: 0,
            ease: Back.easeOut
        });
    }
);

$(".cardWrapper").click(function () {
    $(".cardWrapper").removeClass('active');
    $(this).addClass('active');
});

$('header nav ul li').click(function () {
    /*changePage();*/
    /*glitch();*/
});


/*Mouse Movement*/
(function () {
    // Init
    var container = document.getElementById("banner"),
        inner = document.getElementById("inner-banner");

    // Mouse
    var mouse = {
        _x: 0,
        _y: 0,
        x: 0,
        y: 0,
        updatePosition: function (event) {
            var e = event || window.event;
            this.x = e.clientX - this._x;
            this.y = (e.clientY - this._y) * -1;
        },
        setOrigin: function (e) {
            this._x = e.offsetLeft + Math.floor(e.offsetWidth / 2);
            this._y = e.offsetTop + Math.floor(e.offsetHeight / 2);
        },
        show: function () {
            return "(" + this.x + ", " + this.y + ")";
        }
    };

    // Track the mouse position relative to the center of the container.
    mouse.setOrigin(container);

    //-----------------------------------------

    var counter = 0;
    var updateRate = 10;
    var isTimeToUpdate = function () {
        return counter++ % updateRate === 0;
    };

    //-----------------------------------------

    var onMouseEnterHandler = function (event) {
        update(event);
    };

    var onMouseLeaveHandler = function () {
        inner.style = "";
    };

    var onMouseMoveHandler = function (event) {
        if (isTimeToUpdate()) {
            update(event);
        }
    };

    //-----------------------------------------

    var update = function (event) {
        mouse.updatePosition(event);
        updateTransformStyle(
            (mouse.y / inner.offsetHeight / 2).toFixed(2),
            (mouse.x / inner.offsetWidth / 2).toFixed(2)
        );
    };

    var updateTransformStyle = function (x, y) {
        var style = "rotateX(" + x + "deg) rotateY(" + y + "deg)";
        inner.style.transform = style;
        inner.style.webkitTransform = style;
        inner.style.mozTransform = style;
        inner.style.msTransform = style;
        inner.style.oTransform = style;
    };

    //-----------------------------------------

    container.onmouseenter = onMouseEnterHandler;
    container.onmouseleave = onMouseLeaveHandler;
    container.onmousemove = onMouseMoveHandler;
})();
