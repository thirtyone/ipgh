// noinspection JSAnnotator
import environment from '../environment/env';

export default Object.freeze({
    admin_prefix: environment.admin_prefix,
    base_url: environment.base_url,
    base_api_prefix: environment.base_api_prefix
});