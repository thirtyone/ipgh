<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=375, user-scalable=0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">--}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <title>{{ config('app.name', 'IPGH') }}</title>

    <script>

        var base_url = "{{url('/')}}";
        var prefix = "/";

    </script>

</head>
<body>
<div id="app">
    <navbar-component></navbar-component>
    @include('cores.includes.loader')
    @yield('content')
    <footer-component></footer-component>
</div>


<script src="{{ url('js/app.js') }}"></script>
<script src="{{ url('js/plugins.js') }}"></script>
<script src="{{ url('js/main.js') }}"></script>
<script src="{{ url('js/animation.js') }}"></script>
</body>
</html>
