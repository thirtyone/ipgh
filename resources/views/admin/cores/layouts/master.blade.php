<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link href="{{ mix('administrator/css/app.css') }}" rel="stylesheet">
    <script>

        var base_url = "{{url('/')}}";
        var prefix = "/"+"{{Config::get('urlsegment.admin_prefix')}}";

    </script>
</head>
<body>

<div class="wrapper" id="app">

    <div class="sidebar" data-color="blue" data-image="{{url('administrator/images/bg-sidebar.jpg')}}">
        {{--@include('admin.cores.includes.sidebar')--}}
        <sidebar-component></sidebar-component>
    </div>

    <div class="main-panel">
        <navbar-component></navbar-component>
        @yield('content')

        {{--@include('admin.cores.include
        s.footer')--}}
    </div>


</div>


<!-- Scripts -->
<script src="{{ mix('administrator/js/app.js') }}"></script>


</body>
</html>
