@extends('admin.cores.layouts.master')
<script>
    var token = "{{csrf_token()}}";

</script>
@section('content')


    <div class="content">
        <div class="container-fluid">
            <router-view></router-view>
        </div>
    </div>



@endsection