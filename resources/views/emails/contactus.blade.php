@component('mail::message')
    <b>From:</b> {{ $fullName }} <br>
    <b>Phone:</b> {{ $mobileNumber }} <br>
    <b>Email:</b> {{ $email }} <br>
    <b>Message:</b><br>
    {{ $message }}
@endcomponent
