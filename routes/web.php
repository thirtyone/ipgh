<?php


Auth::routes();


$router->group(['prefix' => '/'], function ($router){

	$router->get('/','PublicPagesController@app');
	// $router->get('mission-and-vision','PublicPagesController@app');
	// $router->get('projects','PublicPagesController@app');
	// $router->get('sectors','PublicPagesController@app');
	$router->get('about','PublicPagesController@app');
	$router->get('partners','PublicPagesController@app');
	$router->get('companies','PublicPagesController@app');
	$router->get('company/{slug}','PublicPagesController@app');
	$router->get('news-and-press','PublicPagesController@app');
	$router->get('news-and-press/{slug}','PublicPagesController@app');
	$router->get('contact','PublicPagesController@app');
	
});


$router->group(['prefix' => \Config::get('urlsegment.admin_prefix'), 'namespace' => 'Admin\Auth'], function ($router){

	$router->get('login', 'LoginController@showLoginForm')->name(\Config::get('urlsegment.admin_prefix').'.login');
	$router->post('login', 'LoginController@login');
	$router->get('logout', 'LoginController@logout');


//	$router->post('admin-password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//	$router->get('admin-password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//	$router->post('admin-password/reset', 'ResetPasswordController@reset');
//	$router->get('admin-password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
});

