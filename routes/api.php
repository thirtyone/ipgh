<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$router->group(['prefix' => "option"], function ($router) {
	$router->get('get-page-content-options', 'OptionsController@getPageContentOption');

});

$router->get('news-presses/with','NewsPressController@indexWithPagination');
$router->resources(
	[
		'companies' => 'CompanyController',
		'partners' => 'PartnerController',
		'page-contents' => 'PageContentController',
		'sectors' => 'SectorController',
		'news-presses' => 'NewsPressController',
		'contacts' => 'ContactController',
		'projects' => 'ProjectController',
        'homepage-tag' => 'HomepageTagController',
        'about' => 'AboutController'
	]

);

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

$router->get('artisan-migrate', function (){
	\Artisan::call('migrate', array('--path' => 'app/migrations', '--force' => true));
	dd(Artisan::output());
});

$router->get('artisan-cache', function (){
	\Artisan::call('config:clear');
	dd(Artisan::output());
});

$router->get('artisan-seed', function (){
    \Artisan::call('db:seed', array('--class' => 'CompanySeeder'));
	\Artisan::call('db:seed', array('--class' => 'ProjectSeeder'));
	dd(Artisan::output());
});
