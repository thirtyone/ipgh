<?php

//
$router->get('/', 'AdminPagesController@index');
$router->get('partners','AdminPagesController@index');
$router->get('partners/create','AdminPagesController@index');
$router->get('partners/{id}/edit','AdminPagesController@index');


$router->get('companies','AdminPagesController@index');
$router->get('companies/create','AdminPagesController@index');
$router->get('companies/{id}/edit','AdminPagesController@index');


$router->get('page-contents','AdminPagesController@index');
$router->get('page-contents/create','AdminPagesController@index');
$router->get('page-contents/{id}/edit','AdminPagesController@index');

$router->get('sectors','AdminPagesController@index');
$router->get('sectors/create','AdminPagesController@index');
$router->get('sectors/{id}/edit','AdminPagesController@index');


$router->get('projects','AdminPagesController@index');
$router->get('projects/create','AdminPagesController@index');
$router->get('projects/{id}/edit','AdminPagesController@index');

$router->get('news-presses','AdminPagesController@index');
$router->get('news-presses/create','AdminPagesController@index');
$router->get('news-presses/{id}/edit','AdminPagesController@index');

$router->get('contacts','AdminPagesController@index');
$router->get('contacts/{id}','AdminPagesController@index');

$router->get('homepage-tags','AdminPagesController@index');
$router->get('about','AdminPagesController@index');

//$router->get('{any:.*}','AdminPagesController@index');
//Route::any('{slug}','AdminPagesController@index');

//Route::get('{any}', 'AdminPagesController@index')->where('any', '.*');

//
//
//$router->resources([
////	'samples' => 'SampleController',
//    'partners' => 'PartnerController',
//]);


$router->group(['prefix' => "api",'namespace'=>"Api"], function($router){

	$router->resources([
		'partners' => 'PartnerController',
		'companies' => 'CompanyController',
		'page-contents' => 'PageContentController',
		'homepage-tags' => 'HomepageTagController',
		'sectors' => 'SectorController',
		'projects' => 'ProjectController',
		'news-presses' => 'NewsPressController',
		'contacts' => 'ContactController',
		'homepage-tag' => 'HomepageTagController',
		'about' => 'AboutController',
	]);

	$router->group(['prefix' => "upload"], function($router){

		$router->post('image', 'UploadController@uploadImage');

	});

});
