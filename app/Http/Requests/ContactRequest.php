<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

	public function messages()
	{
		return [
			'agree.required' => 'You must agree that all the information above is true and accurate.',
//			'phone_number.integer' => 'The phone number must be a number.',

		];
	}


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
	        "first_name" => "required",
	        "last_name" => "required",
	        "email" => "required|email",
	        "phone_number" => "required|min:11",
	        "message" => "required",
	        "agree" => "required",
        ];
    }
}
