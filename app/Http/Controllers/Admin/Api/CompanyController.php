<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\BaseController;
use App\Company;
use App\Repositories\Cms\CmsRepository;
use App\Subcompany;
use Illuminate\Http\Request;


class CompanyController extends BaseController {


	private $cmsRepository;

	public function __construct(Company $company) {
		// set the model
		$this->cmsRepository = new CmsRepository($company);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//

		$data = $request->all();


		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {
			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

// 		$this->validate($request,[
// 			'name' => 'required',
// //			'link' => 'required',
// 			'location' => 'required',
// //			'file_name' => 'required',
//             'description' => 'required',
//             'subcompany.*.name' => 'required',
//             'subcompany.*.description' => 'required'
// 		],[
// //			'file_name.required' => 'Please upload an image',
//             'subcompany.*.name.required' => 'The subcompany name field is required',
//             'subcompany.*.description.required' => 'The subcompany name field is required'
// 	    ]);

		$data = $request->all();

		$subcompany = $data['subcompany'];
        unset($data['subcompany']);
        $data['slug'] = $this->makeSlug($data['name'], 'App\Company');
//		$data['label'] = strtolower($data['name']);
		$res = $this->cmsRepository->create($data);
		$res->subcompany()->createMany($subcompany);
		$request->session()->regenerateToken();

		return response()->json($request->session()->token(), 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->getModel();
		$response = $response->with(['subcompany' => function ($query) {
            $query->orderBy('id');
        }])->find($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

//         $this->validate($request,[
//             'name' => 'required',
// //            'link' => 'required',
//             'location' => 'required',
// //            'file_name' => 'required',
//             'description' => 'required',
//             'subcompany.*.name' => 'required',
//             'subcompany.*.description' => 'required'
//         ],[
// //            'file_name.required' => 'Please upload an image.',
//             'subcompany.*.name.required' => 'The subcompany name field is required',
//             'subcompany.*.description.required' => 'The subcompany name field is required'
//         ]);

		$data = $request->all();
        $subcompany = $data['subcompany'];
        unset($data['subcompany']);
		$this->cmsRepository->update($data, $id);
		Subcompany::where('company_id', $id)->delete();
        $res = Company::find($id);
        $res->subcompany()->createMany($subcompany);
		// $request->session()->regenerateToken();
		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);
		return response()->json(true, 200);


	}
}
