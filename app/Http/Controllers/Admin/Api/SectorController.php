<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\BaseController;


use App\Repositories\Cms\CmsRepository;
use App\Sector;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectorController extends BaseController {

	private $cmsRepository;


	public function __construct(Sector $sector) {
		// set the model
		$this->cmsRepository = new CmsRepository($sector);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();


		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {
			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->orderBy('id','desc')->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request,[
			'name' => 'required',
			'has_many_sector_image.*.file_name' => 'required',
			'description' => 'required',
		],[
			'has_many_sector_image.*.file_name.required' => 'Please upload an image'
        ]);

		$data = $request->all();
		$data['slug'] = $this->makeSlug($data['name'], 'App\Sector');
//		$data['label'] = strtolower($data['name']);
		$sectorImages = $data['has_many_sector_image'];
		unset($data['has_many_sector_image']);
		$response = $this->cmsRepository->create($data);

		foreach ($sectorImages as $key => $value) {
			$response->hasManySectorImage()->create($value);
		}

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->with('hasManySectorImage')->find($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
        $this->validate($request,[
            'name' => 'required',
            'has_many_sector_image.*.file_name' => 'required',
            'description' => 'required',
        ],[
            'has_many_sector_image.*.file_name.required' => 'Please upload an image'
        ]);

		$data = $request->all();
		$sectorImages = $data['has_many_sector_image'];
		unset($data['has_many_sector_image']);
		$response = $this->cmsRepository->show($id);
		$response->hasManySectorImage()->delete();
		$response->fill($data)->save();
		foreach ($sectorImages as $key => $value) {
			$response->hasManySectorImage()->create($value);
		}

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);

		return response()->json(true, 200);


	}
}
