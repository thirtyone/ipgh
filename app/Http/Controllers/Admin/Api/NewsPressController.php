<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\BaseController;
use App\NewsPress;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsPressController extends BaseController {

	private $cmsRepository;

	public function __construct( NewsPress $newsPress) {
		// set the model
		$this->cmsRepository = new CmsRepository($newsPress);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {
			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->orderBy('id','desc')->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$this->validate($request,[
			'name' => 'required',
			'author' => 'required',
			'description' => 'required',
			'file_name' => 'required',
		],[
			'file_name.required' => 'Please upload an image',
		]);
		

		$data = $request->all();
		$data['slug'] = $this->makeSlug($data['name'], 'App\NewsPress');
//		$data['label'] = strtolower($data['name']);

		if(isset($data['featured']) && $data['featured'] == true){
			$this->cmsRepository->getModel()
			->where('featured',1)
			->update([
				'featured' => ''
			]);
		}

		$this->cmsRepository->create($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
        $this->validate($request,[
            'name' => 'required',
            'author' => 'required',
            'description' => 'required',
            'file_name' => 'required',
        ],[
            'file_name.required' => 'Please upload an image',
		]);
		

		$data = $request->all();
		if(isset($data['featured']) && $data['featured'] == true){
			$this->cmsRepository->getModel()
			->where('featured',1)
			->update([
				'featured' => ''
			]);
		}
		$this->cmsRepository->update($data, $id);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);
		return response()->json(true, 200);


	}
}
