<?php

namespace App\Http\Controllers\Admin\Api;

use App\HomepageTag;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageTagController extends Controller
{
    /**
     * @var HomepageTag
     */
    private $homepageTag;

    /**
     * HomepageTagController constructor.
     * @param HomepageTag $homepageTag
     */
    public function __construct(HomepageTag $homepageTag) {
        // set the model
        $this->cmsRepository = new CmsRepository($homepageTag);
        $this->homepageTag = $homepageTag;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->cmsRepository->getModel();
        $response = $response->get()->keyBy('meta_key');

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
			'header_tag.meta_value' => 'required',
			'header_break_tag.meta_value' => 'required',
			'mission_tag.meta_value' => 'required',
			'vision_tag.meta_value' => 'required'
		],[
			'header_tag.meta_value.required' => 'The header tag field is required',
			'header_break_tag.meta_value.required' => 'The header break tag is required',
			'mission_tag.meta_value.required' => 'The mission tag is required',
			'vision_tag.meta_value.required' => 'The vision tag is required'
        ]);

        $data = $request->all();

        $model = $this->cmsRepository->getModel();
        $model->truncate();
        $model->insert($data);

        return response()->json($model, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
