<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\BaseController;
use App\Partner;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends BaseController {

	private $cmsRepository;

	public function __construct(Partner $partner) {
		// set the model
		$this->cmsRepository = new CmsRepository($partner);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();


		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {
			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->orderBy('id','desc')->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

        $this->validate($request,[
            'name' => 'required',
            'file_name' => 'required'
        ],[
            'file_name.required' => 'Please upload an image.'
        ]);
		
		$data = $request->all();

		$data['slug'] = $this->makeSlug($data['name'], 'App\Partner');	

        $this->cmsRepository->create($data);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

		$response = $this->cmsRepository->show($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

        $this->validate($request,[
            'name' => 'required',
            'file_name' => 'required'
        ],[
            'file_name.required' => 'Please upload an image.'
        ]);

		$data = $request->all();
		$this->cmsRepository->update($data, $id);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);

		return response()->json(true, 200);


	}
}
