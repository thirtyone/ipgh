<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\BaseController;

use App\Project;
use App\Repositories\Cms\CmsRepository;
use App\Subproject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends BaseController {

	private $cmsRepository;

	public function __construct(Project $project) {
		// set the model
		$this->cmsRepository = new CmsRepository($project);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//
		$data = $request->all();

		$response = $this->cmsRepository->getModel();

		if (isset($data['keyword'])) {
			$response = $response->where('name', 'LIKE', '%' . $data['keyword']. '%');
		}

		$response = $response->paginate(10);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
//	    return view('admin.app');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
// 		$this->validate($request,[
// 			'name' => 'required',
// 			'file_name' => 'required',
// //			'description' => 'required',
// 			'subproject.*.name' => 'required',
// 			'subproject.*.file_name' => 'required',
// 			'subproject.*.description' => 'required',
// 		],[
// 			'file_name.required' => 'Please upload a project image',
// 			'subproject.*.file_name.required' => 'Please upload a subproject project image',
//         ]);

		// $this->validate($request,[
		// 	'name' => 'required',
		// 	'file_name' => 'required',
		// //			'description' => 'required',
		// 	'subproject.*.name' => 'required',
		// ]);

		$data = $request->all();
        $subproject = $data['subproject'];
        unset($data['subproject']);
		$data['slug'] = $this->makeSlug($data['name'], 'App\Project');
//		$data['label'] = strtolower($data['name']);
        $res = $this->cmsRepository->create($data);
        $res->subproject()->createMany($subproject);

		return response()->json($data, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//

        $response = $this->cmsRepository->getModel();
        $response = $response->with('subproject')->find($id);

		return response()->json($response, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
//         $this->validate($request,[
//             'name' => 'required',
//             'file_name' => 'required',
// //            'description' => 'required',
//             'subproject.*.name' => 'required',
//             'subproject.*.file_name' => 'required',
//             'subproject.*.description' => 'required',
//         ],[
//             'file_name.required' => 'Please upload a project image',
//             'subproject.*.file_name.required' => 'Please upload a subproject project image',
//         ]);

		// $this->validate($request,[
		// 	'name' => 'required',
		// 	'file_name' => 'required',
		// //			'description' => 'required',
		// 	'subproject.*.name' => 'required',
		// ],[
		// 	'subproject.*.name.required' => 'The SubProject name is required',
		// ]);

        $data = $request->all();
        $subproject = $data['subproject'];
        unset($data['subproject']);
        $this->cmsRepository->update($data, $id);
        Subproject::where('project_id', $id)->delete();
        $res = Project::find($id);
        $res->subproject()->createMany($subproject);

		return response()->json($data, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//

		$this->cmsRepository->delete($id);
		return response()->json(true, 200);


	}
}
