<?php

namespace App\Http\Controllers\Admin\Api;

use App\About;
use App\HomepageTag;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * @var HomepageTag
     */
    private $homepageTag;

    /**
     * HomepageTagController constructor.
     * @param HomepageTag $homepageTag
     */
    public function __construct(About $about) {
        // set the model
        $this->cmsRepository = new CmsRepository($about);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->cmsRepository->all();

        foreach ($response as $key => $item) {
            $response[$item->meta_key] = json_decode($item->meta_value);
            unset($response[$key]);
        }
        $response['about_management'] = array_reverse($response['about_management']);
        $response['about_images'] = array_reverse($response['about_images']);
        $response['about_teams'] = array_reverse($response['about_teams']);
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'about.name' => 'required',
        //     'about_images.*.name' => 'required',
        //     'about_images.*.description' => 'required',
        //     'about_images.*.file_name' => 'required',
        //     'about_management.*.name' => 'required',
        //     'about_management.*.description' => 'required',
        //     'about_management.*.position' => 'required',
        //     'about_management.*.path' => 'required',
        //     'about_management.*.file_name' => 'required',
        //     'about_teams.*.name' => 'required',
        //     'about_teams.*.description' => 'required',
        //     'about_teams.*.path' => 'required',
        //     'about_teams.*.file_name' => 'required'
        // ],[
        //     'about.name.required' => 'The about tag field is required',
        //     'about_images.*.name.required' => 'The slider name field is required',
        //     'about_images.*.description.required' => 'The slider description field is required',
        //     'about_images.*.file_name.required' => 'The slider image field is required',
        //     'about_management.*.name.required' => 'The management profile name field is required',
        //     'about_management.*.description.required' => 'The management profile description field is required',
        //     'about_management.*.position.required' => 'The management profile position field is required',
        //     'about_management.*.file_name.required' => 'The management profile image field is required',
        //     'about_teams.*.name.required' => 'The team name field is required',
        //     'about_teams.*.description.required' => 'The team name description is required',
        //     'about_teams.*.file_name.required' => 'The team image field is required'
        // ]);

        $data = $request->all();
        foreach ($data as $key => $datum) {
            $arr = [
                'meta_key' => $key,
                'meta_value' => json_encode($datum)
            ];
            array_push($data, $arr);
            unset($data[$key]);
        }
//        dd($data);
        $model = $this->cmsRepository->getModel();
        $model->truncate();
        $model->insert($data);

        return response()->json($model, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
