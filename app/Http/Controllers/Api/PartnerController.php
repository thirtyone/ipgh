<?php

namespace App\Http\Controllers\Api;

use App\Partner;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
	//

	/**
	 * @var Company
	 */

	private $cmsRepository;


	public function __construct(Partner $partner){

		$this->cmsRepository = new CmsRepository($partner);

	}

	public function index(){

		$response = $this->cmsRepository->getModel()->orderBy('id','desc')->get();
		return response()->json($response,200);
	}

	public function show($slug) {

		$response = $this->cmsRepository->getModel()->whereSlug($slug)->first();
		return response()->json($response,200);
	}
}
