<?php

namespace App\Http\Controllers\Api;

use App\About;
use App\HomepageTag;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
     * @var HomepageTag
     */
    private $homepageTag;

    /**
     * HomepageTagController constructor.
     * @param HomepageTag $homepageTag
     */
    public function __construct(About $about) {
        // set the model
        $this->cmsRepository = new CmsRepository($about);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->cmsRepository->all();
        foreach ($response as $key => $item) {
            $response[$item->meta_key] = json_decode($item->meta_value);
            unset($response[$key]);
        }
        $response['about_management'] = array_reverse($response['about_management']);
        $response['about_images'] = array_reverse($response['about_images']);
        $response['about_teams'] = array_reverse($response['about_teams']);
        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
