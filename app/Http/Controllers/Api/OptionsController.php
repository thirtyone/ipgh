<?php

namespace App\Http\Controllers\Api;

use App\PageContentOption;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionsController extends Controller {
	//


	/**
	 * @var PageContentOption
	 */
	private $pageContentOption;

	public function __construct(PageContentOption $pageContentOption) {

		$this->pageContentOption = $pageContentOption;
	}

	public function getPageContentOption() {
		$response = $this->pageContentOption->all();

		return response()->json($response, 200);
	}
}
