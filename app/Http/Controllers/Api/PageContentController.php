<?php

namespace App\Http\Controllers\Api;

use App\PageContent;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageContentController extends Controller
{
	//

	/**
	 * @var Company
	 */

	private $cmsRepository;
	/**
	 * @var Partner
	 */
	private $partner;


	public function __construct( PageContent $pageContent){

		$this->cmsRepository = new CmsRepository($pageContent);

	}

	public function index(){

		$response = $this->cmsRepository->all();
		return response()->json($response,200);
	}

	public function show($id) {

		$response = $this->cmsRepository->getModel()->wherePageContentOptionId($id)->get();
		return response()->json($response,200);
	}
}
