<?php

namespace App\Http\Controllers\Api;


use App\Repositories\Cms\CmsRepository;
use App\Sector;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SectorController extends Controller {
	//

	/**
	 * @var Company
	 */

	private $cmsRepository;


	public function __construct(Sector $sector) {

		$this->cmsRepository = new CmsRepository($sector);

	}

	public function index() {

		$response = $this->cmsRepository->with(['hasManySectorImage'])->orderBy('id','desc')->get();

		foreach ($response as $key => $res) {
			$res->primary_image = '';
			$res->secondary_image = '';
			foreach ($res->hasManySectorImage as $img) {

				if ($img->primary == 1) {
					$res->primary_image = '/' . $img->path . $img->file_name;

				} else {

					$res->secondary_image = '/' . $img->path . $img->file_name;

				}
			}
			unset($res->hasManySectorImage);

		}

		return response()->json($response, 200);
	}

	public function show($slug) {

		$response = $this->cmsRepository->getModel()->whereSlug($slug)->first();


		return response()->json($response, 200);

	}
}
