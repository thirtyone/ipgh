<?php

namespace App\Http\Controllers\Api;

use App\NewsPress;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsPressController extends Controller
{
	//

	/**
	 * @var Company
	 */

	private $cmsRepository;
	/**
	 * @var NewsPress
	 */
	private $newsPress;


	public function __construct(NewsPress $newsPress){

		$this->cmsRepository = new CmsRepository($newsPress);


	}

	public function index(){

		$response = $this->cmsRepository->all();
		return response()->json($response,200);
	}


	public function indexWithPagination(Request $request){
		$data = $request->all();

		$response =  $this->cmsRepository->getModel()->orderBy('id','desc')->whereFeatured($data['id'])->paginate($data['limit']);

		return response()->json($response, 200);
	}


	public function show($slug) {

		$response = $this->cmsRepository->getModel()->whereSlug($slug)->first();
		if($response){
			$previous =$this->cmsRepository->getModel()->where('id', '<', $response->id)->whereFeatured($response->featured)->first();

			$response->previous_link  = $previous ? $previous->slug : false;

			$next =$this->cmsRepository->getModel()->where('id', '>', $response->id)->whereFeatured($response->featured)->first();

			$response->next_link  = $next ? $next->slug : false;
		}


		return response()->json($response,200);
	}
}
