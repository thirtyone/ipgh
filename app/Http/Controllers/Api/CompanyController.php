<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    //

	/**
	 * @var Company
	 */

	private $cmsRepository;

	public function __construct(Company $company){

		$this->cmsRepository = new CmsRepository($company);
	}

	public function index(){

		$response = $this->cmsRepository->getModel();

        $response = $response->with(['subcompany' => function ($query) {
            $query->orderBy('id');
        }])->get();
		return response()->json($response,200);
	}

	public function show($slug) {

		$response = $this->cmsRepository->getModel()->with('subcompany')->whereSlug($slug)->first();
		return response()->json($response,200);
	}
}
