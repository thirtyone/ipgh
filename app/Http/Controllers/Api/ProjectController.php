<?php

namespace App\Http\Controllers\Api;

use App\Project;
use App\Repositories\Cms\CmsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
	//

	/**
	 * @var Company
	 */

	private $cmsRepository;


	public function __construct(Project $project){

		$this->cmsRepository = new CmsRepository($project);

	}

	public function index(){

		$response = $this->cmsRepository->getModel();
		$response = $response->with('subproject')->get()->keyBy('slug');
		return response()->json($response,200);
	}

	public function show($slug) {

		$response = $this->cmsRepository->getModel()->whereSlug($slug)->first();
		return response()->json($response,200);
	}
}
