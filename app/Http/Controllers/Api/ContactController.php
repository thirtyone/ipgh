<?php

namespace App\Http\Controllers\Api;


use App\Contact;
use App\Http\Requests\ContactRequest;
use App\Repositories\Cms\CmsRepository;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller {
	//

	/**
	 * @var Company
	 */

	private $cmsRepository;
	/**
	 * @var Contact
	 */
	private $contact;


	public function __construct(Contact $contact) {

		$this->cmsRepository = new CmsRepository($contact);

		$this->contact = $contact;
	}
	public function store(ContactRequest $request) {
		$data = $request->all();

		$this->cmsRepository->create($data);

		return response()->json($data, 200);
	}
}
