<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subproject extends Model
{
    //
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
