<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Contact;
class ContactUsEmail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var $contact
     */
    protected $contact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.contactus')
        ->with([
            'message' => $this->contact->message,
            'fullName' => $this->contact->first_name.' '.$this->contact->last_name,
            'mobileNumber' => $this->contact->phone_number,
            'email' => $this->contact->email
        ])
        ->subject('Inquiry')
        ->to(config('services.email.info'))
        ->from($this->contact->email, $this->contact->first_name.' '.$this->contact->last_name);
    }
}
