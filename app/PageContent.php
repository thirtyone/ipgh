<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageContent extends Model
{
    //

	protected $guarded = ['id', 'created_at', 'updated_at'];


	public function pageContentOption() {
	    return $this->belongsTo('App\PageContentOption');
	}
}
