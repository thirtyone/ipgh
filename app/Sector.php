<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    //

	protected $guarded = ['id', 'created_at', 'updated_at'];


	public function hasManySectorImage() {
	    return $this->hasMany('App\SectorImage');
	}
}
