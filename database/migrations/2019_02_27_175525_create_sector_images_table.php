<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectorImagesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('sector_images', function (Blueprint $table) {
			$table->engine = "MyISAM";
			$table->increments('id');
			$table->integer('sector_id')->unsigned();
			$table->foreign('sector_id')
				->references('id')
				->on('sectors')
				->onDelete('cascade');
			$table->string('path');
			$table->string('file_name');
			$table->string('primary');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('sector_images');
	}
}
