<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsPressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_presses', function (Blueprint $table) {
	        $table->engine = "MyISAM";
	        $table->increments('id');
	        $table->string('name');
	        $table->string('label');
	        $table->string('slug');
	        $table->string('featured');
	        $table->string('author');
	        $table->longText('description');
	        $table->string('path');
	        $table->string('file_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_presses');
    }
}
