<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        $data = [
            ["name" => "admin", "email" => "m@m.com", "username" => "admin", "password" => bcrypt('asdfasdf')],
            ["name" => "ipgh admin", "email" => "admin@ipgh.com", "username" => "ipghadmin", "password" => bcrypt('ipghadmin2019')]

        ];
        Admin::insert($data);
    }
}
