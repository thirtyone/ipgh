<?php

use App\PageContentOption;
use Illuminate\Database\Seeder;

class PageContentOptionTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//

		PageContentOption::truncate();
		$data = [
			["name" => "Mission", "label" => "mission", "slug" => "mission"],
			["name" => "Vision", "label" => "vision", "slug" => "vision"],
			["name" => "About Us", "label" => "about us", "slug" => "about-us"],

		];
		PageContentOption::insert($data);

	}
}
