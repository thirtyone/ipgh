<?php

use App\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Project::truncate();
        $data = [
            ["name" => "Afghanistan", "slug" => "afghanistan", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Africa & Middle East", "slug" => "africa", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Benin", "slug" => "benin", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Gabon", "slug" => "gabon", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Indonesia", "slug" => "indonesia", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Iran", "slug" => "iran", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Kuwait", "slug" => "kuwait", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Mauritania", "slug" => "mauritania", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Niger", "slug" => "niger", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Nigeria", "slug" => "nigeria", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Philippines", "slug" => "philippines", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Saudi Arabia", "slug" => "saudi", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Senegal", "slug" => "senegal", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Sri Lanka", "slug" => "srilanka", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Tanzania", "slug" => "tanzania", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "Togo", "slug" => "togo", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
            ["name" => "UAE", "slug" => "uae", "path" => "administrator/images/", "file_name" => "default-avatar.png"],
        ];
        Project::insert($data);
    }
}
