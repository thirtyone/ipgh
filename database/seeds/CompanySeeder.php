<?php

use App\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::truncate();
        $data = [
            ["name" => "Ibayad", "slug" => "ibayad"],
            ["name" => "Ibayar", "slug" => "ibayar"],
            ["name" => "TLC", "slug" => "tlc"],
            ["name" => "DigiAsia BIOS", "slug" => "digiasia-bios"],
        ];
        Company::insert($data);
    }
}
